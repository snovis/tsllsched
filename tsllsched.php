<?php
/*
Plugin Name: TSLL Schedule and Results Tool
Plugin URI: http://tempesouth.com
Description: A Tool for Managing Baseball game Results
Version: 0.61
Author: Scott Novis
Author URI: http://tempesouth.com

Note: this project is now under GIT source management.
2/27/13 07:30:34 PM put in code to filter the display by SEASON.

-------------------------------------------------------------------------------------
 Copyright (C) 2015 Scott Novis <scott@novisware.com>

 TSLLFields free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, version.

 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along 
 with this program. 
 
 If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------


*/
/* 
    3/31/13 02:03:23 PM fix bug with standings.  uses custom field to tell where to link 
        team names in the standings.  field name is schedule-page.  If none is provided
        it defaults to the old 'schedule' page.
    2/27/13 10:18:34 PM add ability access the options table.  Note this table is created
        by TSLLFields project.  The season option was manually created.
*/
define('TSLL_OPTIONS_TABLE', $table_prefix . 'tsll_options');
/**
 * namespace issue, these globals are GLOBAL TO ALL OF WORDPRESS!! So tsllfields is redefining them!
 */

define('TSLLSCHED_PLUGIN_DIR','/wp-content/plugins/' . basename(dirname(__FILE__)));
define('TSLLSCHED_PLUGIN_URI',get_bloginfo('wpurl').TSLLSCHED_PLUGIN_DIR);
define('TSLLSCHED_DATE_MYSQL','Y-m-d');
define('TSLLSCHED_DATE_HUMAN','m/d/Y');
define('TSLLSCHED_DIR',dirname(__FILE__));
// This is key.  The import will be relative to word_press_root/wp-content/plugins/tsllsched
// we need to up 3 levels to get to the wordpress folder.
$relative_path = "/../../..";
define('TSLLSCHED_WP_ROOT_FOLDER',realpath(TSLLSCHED_DIR.$relative_path));
define('TSLLSCHED_IMPORT_DIR_FILE','/tsllsched/import.csv');
define('TSLLSCHED_IMPORT_FILE',TSLLSCHED_WP_ROOT_FOLDER . TSLLSCHED_IMPORT_DIR_FILE );

date_default_timezone_set('America/Phoenix');
ini_set('auto_detect_line_endings', 1);
/**
 * delcare the class if it doesn't exist
 */
if (!class_exists("tsllsched")) {
	class tsllsched {

       protected $tr_class;
       protected $td_class;
       protected $table_class;
       protected $th_class;
       protected $table_action;
       protected $table_action_column;
       protected $displayed_rows;
       protected $season;


	    function tsllsched() { // constructor
            global $wpdb;

            $this->check_tables();  // make sure our tables are installed!
            $this->season = $wpdb->get_var("SELECT TRIM(option_value) FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='season'");   
 
        }

        /**
         * Include our scripts and css if its one of our menu pages.
         */
        function adminHeaders()
        {
            $page = $this->getParam('page');
            if (!empty($page)) {
                $css_url = get_bloginfo('wpurl') . TSLLSCHED_PLUGIN_DIR . '/css/admin.css';
                print "<link rel='stylesheet' type='text/css' href='$css_url' />\n";
                
                // could add a table sorter.
                
                $script_url = get_bloginfo('wpurl') . TSLLSCHED_PLUGIN_DIR . '/js/tsll-admin.js';
                print "<script src='$script_url' type='text/javascript'></script>";
            }

        }
        
        /**
         * addContent
         * core of the application, this generates the content for the user.
         * there are two tags, {TSLLGAMES} - which displays Schedule of Games
         * and {TSLLSTANDINGS} which displays results.
         *
         * @param $content {string} The page content - we scan for our key word
         * @return $content {string}    the HTML content to display.
         */
		function addContent($content = '') {
            $matches = array();
			// if (strpos($content,'{TSLLSCHED}')) {
            if (preg_match('/\{TSLLSCHED:(\d{4})\}/',$content,$matches)) {
                $year = $matches[1];
                $content = $this->render_schedule($content,$year);

            } else {
                $year = date('Y');
                $content = $this->render_schedule($content,$year);  // default is first year.
            }
            
            $matches = array();
            if (preg_match('/\{TSLLSTANDINGS:(\d{4})\}/',$content,$matches)) {
                $year = $matches[1];
                $content = $this->render_standings($content,$year);

            } else {
                $year = date('Y');
                $content = $this->render_standings($content,$year);  // default is first year.
            }            
 
			return $content;
		}

        function render_standings($content,$year) {
            global $wpdb;
            

            $content_path = ABSPATH . TSLLSCHED_PLUGIN_DIR . "/html/standings.phtml";
            // return $content_path;


            /** Need to know where to go if someone clicks on a team **/
            $post_meta = get_post_meta(get_the_ID());
            if (isset($post_meta['schedule-page']))
                $schedule_page = $post_meta['schedule-page'][0];
            else
                $schedule_page = 'schedule';

            $html = file_get_contents( $content_path );


            $majors_table_html = $this->render_standings_table('majors',$year,$schedule_page);

            $html = str_replace('{MAJORS}',$majors_table_html,$html);
            
            $minors_table_html = $this->render_standings_table('minors',$year,$schedule_page);

            $html = str_replace('{MINORS}',$minors_table_html,$html);   

            $matches = array();
            if (preg_match('/\{TSLLSTANDINGS:(\d{4})\}/',$content,$matches)) {
                $content = str_replace($matches[0],$html,$content);            
            } else {
                $content = str_replace('{TSLLSTANDINGS}',$html,$content);
            }            

            return $content;            
            
        }
        
        function render_standings_table($level,$year,$schedule_page) {
            global $wpdb;
            
            $season = "Spring$year";

            $sql = "SELECT 
                        team,
                        played,
                        remaining,
                        CONCAT(wins,'-',losses,'-',ties) record,
                        runs_scored as scored,
                        runs_allowed as allowed,
                        IF(played>0,((wins+(ties/2))/played),0) win_perc
                    FROM (
                    SELECT 
                        team,
                        SUM(runs_scored) runs_scored,
                        SUM(runs_allowed) runs_allowed,
                        count(*) games,
                        SUM(played) played,
                        (COUNT(*)-SUM(played)) remaining,
                        SUM(wins) wins,
                        SUM(losses) losses,
                        SUM(ties) ties
                        
                    FROM (
                    SELECT
                        team,
                        IF(home=team,home_score,away_score) as runs_scored,
                        IF(home=team,away_score,home_score) as runs_allowed,
                        IF(home=team,home_score>away_score,away_score>home_score) as wins,
                        IF(home=team,home_score<away_score,away_score<home_score) as losses,
                        IF(home_score=away_score,1,0) as ties,
                        IF(home_score IS NOT NULL,1,0) as played
                    FROM (
                        SELECT t.team,g.* 
                        FROM wp_tsll_games g 
                        INNER JOIN wp_tsll_team_keys t ON (g.home=t.team OR g.away=t.team)
                        WHERE (g.level = '$level' and g.season='$season') ORDER BY t.team
                    ) as team_list
                    ) as records
                    GROUP BY team) standings
                    ORDER BY win_perc DESC, wins desc, allowed ASC, team ASC";
            $majors = $wpdb->get_results($sql,'OBJECT');
            
            $table_html = "<TABLE>
                            <tr>
                                <th style='vertical-align: bottom;text-align:center;width: '>#</th>
                                <th style='vertical-align: bottom;'>Team</th>
                                <th>Games Played</th>
                                <th>Games Remaining</th>
                                <th style='vertical-align: bottom;'>Record</th>
                                <th style='vertical-align: bottom;'>Win %</th>
                                <th>Runs Scored</th>
                                <th>Runs Allowed</th>
                            </tr>";
                            
            // get link to schedule page
            
            $website_uri = get_bloginfo('wpurl');
            
            
            for ($t = 0; $t < count($majors); $t++) {
                $place = $t+1;
                $team = $majors[$t]->team;
                $played = $majors[$t]->played;
                $remaining = $majors[$t]->remaining;
                $record = $majors[$t]->record;
                $scored = $majors[$t]->scored;
                $allowed = $majors[$t]->allowed;
                $win_perc = $majors[$t]->win_perc;
                $win_perc_str = number_format($win_perc,3);
                $row_html = "<TR>";                
                $row_html .="<TD style='text-align:center;padding: 6px 6px;'>$place</TD>
                            <TD style='padding: 6px 6px;'><a href='$website_uri/$schedule_page/?team=$team'>$team</a></TD>
                            <TD style='text-align: center; padding: 6px 6px;'>$played</TD>
                            <td style='text-align: center; padding: 6px 6px;'>$remaining</td>
                            <TD style='text-align: center; padding: 6px 6px;'>$record</TD>
                            <TD style='text-align: center; padding: 6px 6px;'>$win_perc_str</TD>
                            <TD style='text-align: center; padding: 6px 6px;'>$scored</TD>
                            <TD style='text-align: center; padding: 6px 6px;'>$allowed</TD>";
                $row_html .="</TR>";
                $table_html .= $row_html;
            }
            $table_html .= "</TABLE>";
            return $table_html;
        }
        
        function render_schedule($content,$year) {
            global $wpdb;
            /**
            * Example of how to work with a database
           $sql = "SELECT * FROM " . TSLL_TEAMS_TABLE . " WHERE email='{$current_user->user_email}'";
           $team_info = $wpdb->get_row($sql,ARRAY_A);

            */

            $content_path = ABSPATH . TSLLSCHED_PLUGIN_DIR . "/html/schedule.phtml";
            // return $content_path;

            $html = file_get_contents( $content_path );

            $filters = array(
                'division_filter'=> null,
                'team_filter'=>null,
                'field_filter'=>null,
                'date_filter'=>null,
                'week_filter'=>null,
                'game_filter'=>null,
                'season_filter'=>'Spring'.$year,
                'order'=>'ASC',
                'orderby'=>'game_date'
            );
            
            /**
             * Get default week for the schedule.
             */
            $sql = "SELECT WEEK(game_date,3) FROM wp_tsll_games WHERE season='{$this->season}' ORDER BY game_date ASC LIMIT 1 "; // get first game of season
            $first_game_week = $wpdb->get_var($sql);
            $current_week = date('W');
            $week = ($current_week < $first_game_week) ? $first_game_week : $current_week;
            $sql = "SELECT WEEK(game_date,3) FROM wp_tsll_games WHERE season='{$this->season}' ORDER BY game_date DESC LIMIT 1 "; // get first game of season
            $last_game_week = $wpdb->get_var($sql);
            $week = ($week > $last_game_week) ? $last_game_week : $week;
            
            if (!isset($_REQUEST['filters'])) {
                $filters['week_filter'] = $this->getParam('week',$week);
                $filters['division_filter'] = $this->getParam('division');
                $filters['team_filter'] = $this->getParam('team');
                $filters['field_filter'] = $this->getParam('field');
                $filters['date_filter']= $this->getParam('date');
                $filters['game_filter'] = $this->getParam('game');
                if ($filters['division_filter'] ||
                    $filters['team_filter']     ||
                    $filters['team_filter']     ||
                    $filters['field_filter']    ||
                    $filters['date_filter']     ||
                    $filters['game_filter'])
                    $filters['week_filter'] = null;
            } 

            $filters['order'] = $this->getParam('order','ASC');
            $filters['orderby'] = $this->getParam('orderby','game_date');

            $this->table_class = 'class="game_schedule"';
            $table_html = $this->render_schedule_table($filters);
            $html = str_replace("{TABLE}",$table_html,$html);

            $filter_str = "";
            if ($filters['division_filter'])
                $filter_str .= "[Division] " . $filters['division_filter'];
            if ($filters['team_filter'])
                $filter_str .= '[Team] ' . $filters['team_filter'];
            if ($filters['field_filter'])
                $filter_str .= '[Field] ' . $filters['field_filter'];
            if ($filters['date_filter'])
                $filter_str .= '[Date] ' . $filters['date_filter'];
            if ($filters['week_filter'])
                $filter_str .= '[Week] '. $filters['week_filter'];
            if ($filter_str == "")
                $filter_str = "<em>None</em><small> (Show all Games)</small>";
            $filter_str .= "<br/>Games Displayed: {$this->displayed_rows}";
            $html = str_replace('{FILTERS}',$filter_str,$html);
            
            if (empty($filters['week_filter'])) $filters['week_filter'] = $week;
            $prev_week_link = "?week=".($filters['week_filter']-1);
            $next_week_link = "?week=".($filters['week_filter']+1);
            $html = str_replace('{PREV_WEEK_LINK}',$prev_week_link,$html);
            $html = str_replace('{NEXT_WEEK_LINK}',$next_week_link,$html);
            
            if ($filters['week_filter']) {
                $year = date('Y');
                // Count from '0104' because January 4th is always in week 1
                // (according to ISO 8601).
                $isoTime = strtotime($year . '0104 +' . ($filters['week_filter']-1) . ' weeks');
                $mondayTime = strtotime('-' . date('w', $isoTime) . ' days',  $isoTime);            
                $current_week = date('M jS, Y',$mondayTime);
                $html = str_replace('{CURRENT_WEEK}',$current_week,$html);                
            } else {
                $html = str_replace('{CURRENT_WEEK}','<em>all dates</em>',$html);                  
            }

            $matches = array();
            if (preg_match('/\{TSLLSCHED:(\d{4})\}/',$content,$matches)) {
                $content = str_replace($matches[0],$html,$content);
            } else {
                $content = str_replace('{TSLLSCHED}',$html,$content);
            }

            return $content;
        }
        /**
         * This function is responsible for verifying that the database tables
         * we want exist.  If they don't... we create them.
         */
        function check_tables()  {
            global $wpdb;  // pointer to the database object
            // get code from tsllfields to handle table initialization

        }



        /**
         * Create a menu for this plugin
         */
        function addMenu()
        {
            if (function_exists('add_menu_page'))  {
                add_menu_page('TSLL Schedule','TSLLSchedule','manage_options',__FILE__,array(&$this, 'displayMenuPage'), TSLLFIELDS_URI . "/images/menu.gif");
                $this->summary_page = add_submenu_page(__FILE__,'Game Schedule','Summary','manage_options',__FILE__,array(&$this, 'displayMenuPage'));
                // $this->team_page = add_submenu_page(__FILE__,'Manage Teams','Manage Teams','manage_options','tsll-assign-teams',array(&$this, 'displayMenuManageTeams'));
                $this->games_page = add_submenu_page(__FILE__,'Manage Games','Manage Games','manage_options','tsll-results',array(&$this, 'displayMenuManageResults'));
                $this->db_page = add_submenu_page(__FILE__,'Manage Data','Manage Data','manage_options','tsll-data',array(&$this, 'displayMenuManageData'));
                //$this->results_page = add_submenu_page(__FILE__,'Manage Game Times','Manage Game Times','manage_options','tsll-games',array(&$this, 'displayMenuManageGameTimes'));
                //$this->options_page = add_submenu_page(__FILE__,'Manage Options','Manage Options','manage_options','tsll-options',array(&$this, 'displayMenuOptions'));
            }
        }

        /**
         * Display the menu Schedule
         */
        function displayMenuPage() {
            global $wpdb;

            $view = new stdClass();
            $view->request_path = get_option('siteurl') . '/wp-admin/admin.php?page=tsllsched/tsllsched.php';
            // if we want navigation buttons's we're going to have to create them.
            $view->html = "<p>Create overview statistics about the schedule</p>";

            // We need to know which season we are looking at...
            //$season = $wpdb->get_var("SELECT TRIM(option_value) FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='season'");   
            $season = $this->season;

            include_once('html/main.phtml');
            $this->display_schedule_stats($season);
        }

        /**
         * This function will handle data related to assigning TEAM post_types to scheduled teams.
         */
        function displayMenuManageTeams() {
            print "<div class='wrap'><div id='icon-fields' class='icon32'></div><h2>Review Team Reservation Totals</h2></div><br/>";
            $view->request_path = get_option('siteurl') . '/wp-admin/admin.php?page=tsll-assign-teams';
        }

        function displayMenuManageResults() {
            print "<div class='wrap'><div id='icon-fields' class='icon32'></div><h2>Edit Game Results</h2></div><br/>";

            $view = new stdClass();
            $view->action = null;
            $view->action = $this->getParam('submit');


            //$view->test = 'Default';
            if ("View" == $view->action || $view->action == null) {
                $this->display_games_list();
            } elseif ("edit" == $view->action ) {
                $this->edit_game();
            } elseif ("Update" == $view->action) {
                $this->update_game();
            }
        }

		function displayMenuManageData() {

            $view = new stdClass();
            $view->action = $this->getParam('action');


			print "<div class='wrap'><div id='icon-fields' class='icon32'></div><h2>Manage Database</h2></div><br/>";
			include_once('html/manage_data.phtml');


            switch ($view->action) {
                case 'reset': $this->reset_game_data(); break;
                case 'team_list': $this->generate_team_list(); break;
                case 'import': $this->import_schedule(); break;
                default: $this->display_schedule_stats($this->season); break;
            }

		}

        /**
         * Default action
         * Display the list of games.
         */
        function display_games_list() {
            $view = new stdClass();
            $view->request_path = get_option('siteurl') . '/wp-admin/admin.php?page=tsll-results';
            $this->tr_class = array('field'=>'level');
            $this->table_class = 'class="widefat"';
            $view->level = $this->getParam('level',null);
            $this->table_action = $view->request_path . "&submit=edit&level={$view->level}&id=";
            $this->table_action_column = 'home';
            $columns = array('id','league','level','home','away','game_date','game_time','field','home_score','away_score');

            if ($view->level != null) {
                $where = "level='{$view->level}'";  
            } else {
                $where = "1";
            }

        
            $view->table_html = $this->query_to_table('wp_tsll_games',$columns,$where,'level, game_date, game_time, field','');
            include_once('html/results.phtml');
        }

        function display_schedule_stats($season) {
            global $wpdb;

            /*
               Note: we need to know which season we are in, so use the tsll_options table
               to tell us which season to focus on.  Make it friendlier later.
            */
            $this->table_class = 'class="widefat"';
            $view = new stdClass();
            $sql_str = "SELECT home AS team,
                            level AS division,
                            count(*) as home_games
                        FROM wp_tsll_games
                        WHERE season='$season'
                        GROUP BY home
                        ORDER BY FIELD(division,'majors','minors','aaa','aa'),team;";
            $view->rows = $rows = $wpdb->get_results($sql_str,'OBJECT');
            $html = $this->results_to_table($view->rows);
            $sql_str = "SELECT count(*) FROM wp_tsll_games WHERE season='$season';";
            $game_count = $wpdb->get_var($sql_str);
            print "<p>Total Number of Games: $game_count</p>";
            print $html;
        }

        function edit_game() {
            global $wpdb;

            $id = $this->getParam('id');

            // Let's edit the record the user specified
            $view = $wpdb->get_row("SELECT * FROM wp_tsll_games WHERE id=$id",'OBJECT');
            $view->form_action = $_SESSION['REQUEST_URI'];
            $view->level = $this->getParam('level');            

            include_once('html/edit_game.phtml');
        }

        function update_game() {
            global $wpdb;
            $id = $this->getParam('id');
            $home_score = $this->getParam('home_score');
            $away_score = $this->getParam('away_score');
            $notes = stripslashes($_REQUEST['notes']);
            
            if ($home_score==-1) {
                $wpdb->query ($wpdb->prepare ("UPDATE wp_tsll_games SET home_score=NULL, away_score=NULL, notes=NULL WHERE id=$id"));                
            } else {
                $wpdb->update('wp_tsll_games',
                              array(
                                'home_score'=>$home_score, // integer
                                'away_score'=>$away_score, // integer
                                'notes'=>$notes  // string
                              ),
                              array ('id' => $id),
                              array (   // update formatting
                                '%d',       // for first value as an integer
                                '%d',       // for second value as an integer
                                '%s'        // for notes
                              ),
                              array( '%d') // where format
                        );                
            }

            $this->display_games_list();
        }
        /**
         * headerInit
         * make sure jQuery is queued up.
         */

        function load_scripts_and_styles() {
            

            if (!is_admin()) {               
                if (is_page('Schedule',139,'schedule')) {
                    wp_enqueue_script('jquery');  // make sure this gets loaded.
                    wp_enqueue_script('jquery-ui-core');
                    wp_enqueue_script('jquery-ui-dialog');
                    
                    // register my script then load it.
                    $script_path = TSLLSCHED_PLUGIN_URI . '/js/script01.js';
                    wp_register_script('tsll-sched',$script_path,array('jquery','jquery-ui-core'));
                    wp_enqueue_script('tsll-sched');

                    wp_register_style( 'jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/smoothness/jquery-ui.css', true);
                    wp_enqueue_style('jquery-style');
                    
                    $style_path = TSLLSCHED_PLUGIN_URI . '/css/style.css';
                    wp_register_style('tsll-sched',$style_path,true);
                    wp_enqueue_style('tsll-sched');
                   
                  
                }   
            }
		}


        /**
         * Handle the front end user requests via Ajax
         */
        function userRequest() {
            global $wpdb;
            global $current_user;
            get_currentuserinfo();

            $action = $this->getParam('tsll_action');
            $response = array(success=>false,html=>"");

            switch ($action) {
                case 'StartRequest':

                    break;

                case 'request':
                    /**
                     * This is pretty tricky stuff.  I need to work on a test case for all situations.
                     */
                    break;

                default:
                    $response['success'] = false;
                    $response['html'] = "Invalid Request.  Please try again.";
            }
            die(json_encode($response));
        }
        /*
         * Gets a passed parameter or returns the default
         * ZendLike
         */
        function getParam($var,$default=null,$escaped=true) {
           $value = isset($_REQUEST[$var]) ? $_REQUEST[$var] : $default;
           if ($escaped) $value = mysql_escape_string($value);
           return $value;
        }

        /**
         * Not every table will be built this way, but this is consistent with
         * the fields application.
         */
        function query_to_table($table,$columns,$where='1',$orderby=null,$asc='ASC') {
            global $wpdb;

            $column_str = implode(",",$columns);
            if (empty($orderby)) $orderby = $columns[0];

            $sql_str = "SELECT $column_str FROM $table WHERE season='{$this->season}' AND $where ORDER BY $orderby $asc";
            $rows = $wpdb->get_results($sql_str,'OBJECT');

            // Results are an array of objects.
            $html = "<table id='table_$table' {$this->table_class}>";
            $headers_html = $this->array_to_row($columns,'TH');
            $html .= "<thead>$headers_html</thead>";
            $html .= "<tfoot>$headers_html</tfoot>";
            $html .= "<tbody>";
            for ($r = 0; $r < count($rows); $r++) {
                $html .= $this->object_to_row($rows[$r]);
            }
            $html .= "</tbody>";
            $html .="</table>";
            return $html;
        }

        /**
         * convert an array of objects (or arrays)
         * into an html table.
         */
        function results_to_table($rows) {
            if (count($rows)==0) {
                $html = "<p>No Games Loaded For {$this->season}</p>";
                return $html;
            }
            $members= get_object_vars($rows[0]);
            $columns = array_keys($members);


            // Results are an array of objects.
            $html = "<table id='table_results' {$this->table_class}>";
            $headers_html = $this->array_to_row($columns,'TH');
            $html .= "<thead>$headers_html</thead>";
            $html .= "<tfoot>$headers_html</tfoot>";
            $html .= "<tbody>";
            for ($r = 0; $r < count($rows); $r++) {
                $html .= $this->object_to_row($rows[$r]);
            }
            $html .= "</tbody>";
            $html .="</table>";
            return $html;
        }

        function array_to_row($columns,$tag = 'TD') {
            $row_html = "<tr>";
            for ($c = 0; $c < count($columns); $c++) {
                $cell = "<$tag>".ucwords($columns[$c])."</$tag>";
                $row_html .= $cell;
            }
            $row_html .= "</tr>";
            return $row_html;
        }

        function object_to_row(&$obj,$tag = 'TD') {

            if (is_array($this->tr_class)) {
                $field = $this->tr_class['field'];               
                $value = $obj->{$field};
                $class = " class='$value' ";
            } else if (!empty($tr_class)) {
                $class = " class='{$this->tr_class}' ";
            } else {
                $class = "";
            }
            
            $row_html = "<tr$class>";
            foreach ($obj as $key => $value) {
                if ($this->table_action && ($key == $this->table_action_column))
                    $value = "<a href='{$this->table_action}".$obj->id."'>$value</a>";
                $cell = "<$tag>$value</$tag>";
                $row_html .= $cell;
            }
            $row_html .= "</tr>";
            return $row_html;
        }

        /**
         * This table function has to be pretty robust.  It needs to generate an
         * HTML table based upon certain constraints.  Namely, we will want to
         * filter on:
         *
         * Division
         * (season?)
         * Team
         * Field
         * And Dates (like by default we will only want to show up coming games)
         *
         * So let's start with the division filter.
         */
        function render_schedule_table($filters) {
            global $wpdb;

            $table = 'wp_tsll_games';
            $columns = array('home','away','game_date','game_time','field','home_score','away_score');
            $labels = array('Home Team','Away Team','Date','Time','Field','Home Score','Away Score','Notes');
            $where = 1;
            $path = $_SERVER['PATH_TRANSLATED'];

            extract($filters);

            $column_str = implode(",",$columns);
            if (empty($orderby)) $orderby = $columns[0];
            
            if (!empty($week_filter)) {
                $where = "WEEK(game_date)=".(int)($week_filter-1);
            }
            
            if (!empty($division_filter)) {
                $where = "level='$division_filter'";
            }
            
            if (!empty($team_filter)) {
                // note team filter overrides all other filters!
                $where = "(home='$team_filter' OR away='$team_filter')";
            }
            
            if (!empty($field_filter))
                $where = "field='$field_filter'";
                
            if (!empty($date_filter))
                $where = "game_date='$date_filter'";

            $sql_str = "SELECT id,$column_str,notes FROM $table 
                WHERE season = '$season_filter' 
                AND $where ORDER BY $orderby $order,game_time,field";

            $rows = $wpdb->get_results($sql_str,'OBJECT');

            // Results are an array of objects.
            // I figured it out.  There were no line breaks and that caused the html
            // string to be too LONG for the production server.  The line breaks fixed the crash.
            $count = count($rows);
            $this->displayed_rows = $count;
            $html = "<table id='table_$table' {$this->table_class}>";
            $headers_html = $this->array_to_row($labels,'TH');
            $html .= "<thead>$headers_html</thead>";
            $html .= "<tfoot>$headers_html</tfoot>";
            $html .= "<tbody>";
             for ($r = 0; $r < count($rows); $r++) {
                $row_id = $rows[$r]->id;
                $html .= "<tr id='$row_id'>\n";

                $home = $rows[$r]->home;
                $away = $rows[$r]->away;
                $home_link = "<a href='$path?team=$home'>$home</a>";
                $away_link = "<a href='$path?team=$away'>$away</a>";
                $html .= "<td style='padding:2px;'>{$home_link}</td>\n";
                $html .= "<td style='padding:2px;'>{$away_link}</td>\n";

                $game_date = $rows[$r]->game_date;
                $game_time = $rows[$r]->game_time;
                $udate = strtotime($game_date . " " .$game_time);
                $date_str = date('D, M-d',$udate);
                $time_str = date('g:i A',$udate);
                $date_link = "<a href='$path?date=$game_date'>$date_str</a>";
                $html .= "<td class='game_date' style='padding:2px;'>{$date_link}</td>\n";
                $html .= "<td style='padding:2px;'><div style='display: block; width: 96px; text-align: center;padding:2px;'>{$time_str}</div></td>\n";
                $field = $rows[$r]->field;
                $field_link = "<a href='$path?field=$field'>$field</a>";
                $html .= "<td class='center' style='padding:2px;'>{$field_link}</td>\n";
                $html .= "<td class='score' style='padding:2px;'>{$rows[$r]->home_score}</td>\n";
                $html .= "<td class='score' style='padding:2px;'>{$rows[$r]->away_score}</td>\n";
                
                if (!empty($rows[$r]->notes)) {
                    $notes = str_replace("\n","<br/>",$rows[$r]->notes);
                    $html .= "<td class='center' style='padding:2px;'>
                                <img class='hasnotes' src='".TSLLSCHED_PLUGIN_URI.'/images/application_form.png'."'/>
                             <div id='notes_$row_id'class='hidden'>{$notes}</div>   
                             </td>";                    
                } else {
                    $note = "";
                    $html .= "<td class='center' style='padding:2px;'>
                                &nbsp;
                                <div id='notes_$row_id'class='hidden'>{$notes}</div>   
                             </td>";                       
                }


                $html .="</tr>\n";
            }

            $html .= "</tbody>";
            $html .="</table>";

            $html .="<br/><div id='sub_totals'>Games Displayed: $count</div>";

            return $html;
        }

        /**
         * reset's the game data table.
         * I may not need this.  Why not reset on import?
         */
        function reset_game_data() {
            global $wpdb;

            print "Reset the game data!<br/>\n";
            $count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM wp_tsll_games;" ) );

            $confirm = $this->getParam('confirm');
            if ($confirm == 'confirm') {
                $wpdb->query("DELETE FROM wp_tsll_games WHERE 1;");  // blow it ALL away...
                print "$count Records Deleted.<br/>";
            } else {
                print "Really Drop $count game records?<br/>\n";
                print  "<form name='delete_them_all' method='POST' action='{$_SERVER['REQUEST_URI']}'>
                            <input type='hidden' name='page' value='tsll-data'/>
                            <input type='hidden' name='action' value='reset'/>
                            <input type='submit' name='confirm' value='confirm'/>
                        </form>";
            }

        }

        /**
         * Troll the schedule for team names and populate the team database with
         * the team names.
         *
         * Note: This relies on the idea that EVERY team has at least ONE home game.
         */
        function generate_team_list() {
            global $wpdb;
            print "<h2>Generate List of teams</h2><br/>\n";

            $confirm = $this->getParam('confirm');
            if ($confirm == 'confirm') {
                print "Rebuilding Team List from Scheduled Games<br/>\n";
                $data = $wpdb->get_col("SELECT DISTINCT home from wp_tsll_games WHERE season='{$this->season}' ORDER BY home ASC");
                $wpdb->query("DELETE FROM wp_tsll_team_keys WHERE 1");
                for ($t=0; $t < count($data); $t++) {
                    $wpdb->insert('wp_tsll_team_keys',array('team'=>$data[$t]));
                }
                print "Team List Generated<br/>\n";
                print "<pre>\n";
                print_r($data);
                print "</pre>\n";
            } else {
                print "Rebuild Team List?<br/>\n";
                print  "<form name='delete_them_all' method='POST' action='{$_SERVER['REQUEST_URI']}'>
                            <input type='hidden' name='page' value='tsll-data'/>
                            <input type='hidden' name='action' value='team_list'/>
                            <input type='submit' name='confirm' value='confirm'/>
                        </form>";
                print "<hr/>Current List:<br/>\n";
                $teams = $wpdb->get_col('SELECT team from wp_tsll_team_keys');
                print "<pre>\n";
                print_r($teams);
                print "</pre>\n<hr/>\n";
            }


        }

        /**
         * import schedule
         * import a csv with the schedule into the database.
         */
        function import_schedule() {
            global $wpdb;

            print "<h2>Import Schedule</h2><br/>\n";

            $confirm = $this->getParam('confirm');
            if ($confirm == 'confirm') {
                print "<p>Importing Games...</p>\n";

                $row = 0;
                $handle = fopen(TSLLSCHED_IMPORT_FILE,"r");
                if ($handle != false) {
                    while (($data = fgetcsv($handle,1000, ",")) != false) {
                        $row++;
                        print ".";
                        if ($row < 4) continue;  // skip the first 3 lines
                        $num = count($data);
                        if ($num < 5) {
                            print "<span style='color:red'>Bad Record!</span>\n";
                            echo "<p> $num fields in line $row: <br/></p>\n";
                            for ($c=0; $c < $num; $c++) {
                                echo $data[$c] . "<br/>\n";
                            }
                        } else {
                            // row contains a valid game.
                            $game = new stdClass();
                            $game->home = $data[0];
                            $game->away = $data[1];
                            $game->field = $data[2];
                            $date = $data[3];
                            $time = $data[4];
                            $matches = array();
                            preg_match('/(\d{1,2}).(\d{1,2}).(\d{2})/',$date,$matches, PREG_OFFSET_CAPTURE);
                            $month = sprintf("%02d",$matches[1][0]);
                            $day = sprintf("%02d",$matches[2][0]);
                            $year = "20".$matches[3][0];
                            $game->game_date = "$year-$month-$day";
                            $game->game_time = $time;

                            // Clean up the divisions
                            $game->level = 'majors';  // default.
                            if (substr($game->home,0,2) == "m_") {
                                $game->level = 'minors';
                            } else if (substr($game->home,0,4) == "aaa_") {
                                $game->level = 'aaa';
                            } else if (substr($game->home,0,3) == "aa_") {
                                $game->level = 'aa';
                            } else if (substr($game->home,0,3) == "tb_") {
                                $game->level = 'tball';
                            }
                            $game->season = 'Spring'.$year; // new database feature.

                            print "<pre>\n";
                            print_r($game);
                            print "</pre>\n";
                            // Now convert it to something useful
                            $record = array(
                                'league' => 'TSLL',
                                'season'=> $game->season,
                                'level' => $game->level,
                                'home' => $game->home,
                                'away' => $game->away,
                                'game_date' => $game->game_date,
                                'game_time' => $game->game_time,
                                'field' => $game->field
                            );
                            $wpdb->insert('wp_tsll_games',$record);
                        }
                    }
                }
                fclose($handle);
                print "<p>finished import.</p>\n";
            } else {
                print "Import Schedule Games?<br/>\n";
                print "<p>Note file is <code>import.csv</code> in <code>/tsllsched</code> directory</p>";
                print  "<form name='import_games' method='POST' action='{$_SERVER['REQUEST_URI']}'>
                            <input type='hidden' name='page' value='tsll-data'/>
                            <input type='hidden' name='action' value='import'/>
                            <input type='submit' name='confirm' value='confirm'/>
                        </form>";
            }


        }

    } // End Class Constructor


} // End if

/**
 * AJAX Functions
 *
 * These can't be inside an object - because the object might not be instantiated.
 */

/**
 * tsll_field_request
 * User requests a field
 */
function tsll_user_request()
{
    global $tsllsched;

    $tsllsched->userRequest();

}

function tsll_schedule_admin_menu() {

}

/**
 * Instantiate the class if class has been delcared
 */
if (class_exists("tsllsched")) {
	$tsllsched = new tsllsched();
}

// Hook the Actions and Filters
if (isset($tsllsched)) {

    // Actions
    add_action('admin_menu',array(&$tsllsched, 'addMenu'));
	//add_action('init',array(&$tsllsched, 'headerInit'));
    add_action('wp_head',array(&$tsllsched, 'load_scripts_and_styles'));
    add_action('admin_head',array(&$tsllsched, 'adminHeaders'));    

    // AJAX actions
    add_action('wp_ajax_tsll_user_request',tsll_user_request);

	// Filters
	add_filter('the_content',array(&$tsllsched, 'addContent'));
    // add_action('admin_bar_menu', 'tsll_schedule',1000);

}

/**
 * Handle when the plugin is deactivated.
 */

function tsllsched_deactivate() {
	// For now wipe the databases.
	global $wpdb;

	//$wpdb->query("DROP TABLE IF EXISTS " . TSLL_FIELDS_TABLE);

}
register_deactivation_hook( __FILE__, 'tsllsched_deactivate' );

