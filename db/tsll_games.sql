# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.1.44)
# Database: tsbb
# Generation Time: 2012-02-06 06:38:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table wp_tsll_games
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_tsll_games`;

CREATE TABLE `wp_tsll_games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `league` varchar(32) DEFAULT NULL,
  `level` varchar(32) DEFAULT NULL,
  `home` varchar(32) DEFAULT NULL,
  `away` varchar(32) DEFAULT NULL,
  `game_date` date DEFAULT NULL,
  `game_time` time DEFAULT NULL,
  `field` varchar(32) DEFAULT NULL,
  `home_score` int(11) DEFAULT NULL,
  `away_score` int(11) DEFAULT NULL,
  `notes` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wp_tsll_games` WRITE;
/*!40000 ALTER TABLE `wp_tsll_games` DISABLE KEYS */;

INSERT INTO `wp_tsll_games` (`id`, `league`, `level`, `home`, `away`, `game_date`, `game_time`, `field`, `home_score`, `away_score`, `notes`)
VALUES
	(1,'TSLL','majors','RedSox','Cubs','2011-03-26','17:30:00','TS3',3,3,NULL),
	(2,'TSLL','majors','WhiteSox','As','2011-03-29','19:30:00','TS3',13,2,'Jake: 96\r\nFlabby: 3'),
	(3,'TSLL','minors','m_RedSox','m_Astros','2011-03-26','17:30:00','TS6',NULL,NULL,NULL),
	(4,'TSLL','minors','m_Giants','m_Cardinals','2011-03-29','17:30:00','TS6',NULL,NULL,NULL),
	(5,'TSLL','majors','RedSox','WhiteSox','2011-03-05','17:30:00','TS3',8,1,'We kicked their butts!'),
	(6,'TSLL','majors','Cubs','As','2011-03-05','17:30:00','TSC4',5,8,'Lots of pitches'),
	(7,'TSLL','majors','DBacks','Dodgers','2011-03-05','19:30:00','TSC4',7,7,'Phew');

/*!40000 ALTER TABLE `wp_tsll_games` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
