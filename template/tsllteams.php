<?php
/*
Template Name: TsllTeams

Usage:
To use this template, there must be a custom post type named tsllteam.
There must also be a custom taxonomy called tsll-leagues and a custom
field called tsll_season.

Posts are sorted and filtered by these values.

-------------------------------------------------------------------------------------
 Copyright (C) 2015 Scott Novis <scott@novisware.com>

 TSLLFields free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, version.

 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along 
 with this program. 
 
 If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
  
*/

get_header(); ?>

    <div id="container">
        <div id="content" role="main">
        <h1>Spring 2012 Teams</h1>
        <?php
        $divisions = array('majors','minors','aaa','aa');
        foreach ($divisions as $division) {
        ?>
            <h3><?= strtoupper($division); ?></h3>
            
            <?php
            //$teams = new WP_Query(array('post_type' => 'tsllteam','tsll-leagues'=>'Majors'));
            //$teams = new WP_Query('post_type=tsllteam');
            $args = array (
               'post_type' => 'tsllteam',
               'orderby'=>'name',
               'order'=>'asc',
               //'author_name' => 'Scott',
               'tax_query' => array(
                    array(
                        'taxonomy' => 'tsll-leagues',
                        'terms'=>array($division),
                        'field'=>'slug'
                    )
                ),
               'meta_query'=>array(
                    array(
                        'key'=>'tsll_season',
                        'value'=>'Spring2012'
                    )
               )
    
               );
            $teams = new WP_Query($args);
            while ($teams->have_posts()) : $teams->the_post();
            ?>
            <strong><a href="<?= get_permalink(); ?>"><?php the_title(); ?></a></strong>
            <?php
            $manager = get_post_meta($post->ID,'tsll_manager',true);
            $division = get_post_meta($post->ID,'tsll_division',true);
            echo " :"; echo $post->ID;
            echo ":"; echo $manager;
            echo ":"; echo $division;
            ?>
            <hr/>
            <?php endwhile; wp_reset_query();
        } // and foreach $division
        ?>

        </div><!-- #content -->
    </div><!-- #container -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
