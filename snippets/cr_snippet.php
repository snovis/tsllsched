<?php
	/**
	 you can convert between arrays and objects
	output:
	Array
	(
	    [key] => value
	    [foo] => bar
	)
	stdClass Object
	(
	    [hot] => digity
	    [dog] => hot
	)
	Array
	(
	    [hot] => digity
	    [dog] => hot
	)
	stdClass Object
	(
	    [key] => value
	    [foo] => bar
	)
	*/
	$a = array('key'=>'value','foo'=>'bar');
	$o = new stdClass();
	$o->hot = 'digity';
	$o->dog = 'hot';
	print_r($a);
	print_r($o);
	
	$c = (array)$o;
	$d = (object)$a;
	print_r($c);
	print_r($d);
	
	print "Document Root: ". $_SERVER['DOCUMENT_ROOT']."\n";
	print "Document : ". __FILE__ ."\n";
	$dir = dirname(__FILE__);
	print "Dirname: " . $dir ."\n";
	$up_a_couple = $dir . "/../..";
	print "RealPath: ". realpath($up_a_couple)."\n";
?>