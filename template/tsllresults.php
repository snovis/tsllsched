<?php
/*
Template Name: TsllResults

Usage:
This template can be used to display results on a page...

Posts are sorted and filtered by these values.

-------------------------------------------------------------------------------------
 Copyright (C) 2015 Scott Novis <scott@novisware.com>

 This application is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, version.

 This application is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along 
 with this application. 
 
 If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
 
*/

get_header(); ?>

    <div id="container">
        <div id="content" role="main">
        <h1>Game Results</h1>
        <form name="tsllform">
            <select name="season">
                <option value="Spring2011">Spring 2011</option>
                <option value="Spring2012">Spring 2012</option>
            </select>
            <input type="submit" value="submit" />
        </form>

        <?php
        $game = $wpdb->get_row('select * from wp_tsll_games where id=1','OBJECT');
        print_r($game);
        
        if (isset($tsllsched)) { echo "<br/><p>TSLLSCHED is here!</p>"; } else { echo "<br/>No Dice muchacho"; }
        ?>
        <p>Game Date is: <?= $game->gamedate?></p>
        <br/>
        The Season is: <strong><?= $_REQUEST['season'] ?></strong>
        </div><!-- #content -->
    </div><!-- #container -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
