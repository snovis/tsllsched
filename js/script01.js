/**
 * Functions for managing practice plan lists
-------------------------------------------------------------------------------------
 Copyright (C) 2015 Scott Novis <scott@novisware.com>

 This application is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, version.

 This application is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along 
 with this application. 
 
 If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
 
 */
$j = jQuery.noConflict();
 

$j(document).ready(function($) {
    function DisplayNotes(obj) {
        var img = $(this);
        var td = img.parent();
        var tr = td.parent();
        var cells = tr.find('TD');
        
        var id = tr.attr('id');
        var note_div = $('#notes_'+id);
        var notes = note_div.html();
        var home_score = $(cells[5]);
        var home_team = $(cells[0]);
        //var home_team = home_score.parent().children().first();
        var away_score = $(cells[6]);
        var away_team = $(cells[1]);
        if (notes == "") notes = "<em>No Notes Yet</em>";
        var dlg = $('#game_detail');
        var msg = "<p>[H] <strong>"+home_team.text()+" : </strong>"+home_score.text()+"<br/>";
        msg += "[V] <strong>"+away_team.text()+" : </strong>"+away_score.text()+"</p>";
        msg +=   "<hr/><p>"+notes+"</p>";      
        dlg.html(msg);
        dlg.dialog("option","title","Game Details For Game: "+id);
        dlg.dialog('open');
    }
    
    $('img.hasnotes').click(DisplayNotes);
    $('#game_detail').dialog({
        autoOpen: false,        
        modal: true,
        buttons: { "Ok": function (){ $(this).dialog("close"); }}
    });
});
