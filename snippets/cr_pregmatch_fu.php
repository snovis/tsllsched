<?php
	date_default_timezone_set('America/Phoenix');
	$date="4/1/13";
	 $matches = array();
	 preg_match('/(\d{1,2}).(\d{1,2}).(\d{2})/',$date,$matches, PREG_OFFSET_CAPTURE);
	 $month = sprintf("%02d",$matches[1][0]);
	 $day = str_pad($matches[2][0],2,"0",STR_PAD_LEFT);
	print "Month: $month Day: $day\n";
	print date('W',strtotime("2013-03-23"));
?>