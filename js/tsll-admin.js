/**
 * Functions for managing practice plan lists
 * 
 -------------------------------------------------------------------------------------
  Copyright (C) 2015 Scott Novis <scott@novisware.com>
 
  This application is free software: you can redistribute it and/or modify it under the terms 
  of the GNU General Public License as published by the Free Software Foundation, version.
 
  This application is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
  
  See the GNU General Public License for more details.
  You should have received a copy of the GNU General Public License along 
  with this application. 
  
  If not, see <http://www.gnu.org/licenses/>.
 -------------------------------------------------------------------------------------
  
 */
$j = jQuery.noConflict();

function setCookie(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==c_name) {
            return unescape(y);
        }
    }
    return "";
}

$j(document).ready(function($) {
    $('TR.majors TD').css('border-color','purple');
    $('TR.minors TD').css('border-color','yellow');
    $('TR.aaa TD').css('border-color','#00E800');
    $('TR.aa TD').css('border-color','darkgreen');
});
