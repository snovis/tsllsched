README TSLL
==========

This project is pretty complicated as it does several things.  The main components are:

* The tsbb3 template
  * tsllresults.php
  * tsllteams.php
* The tsllfields plugin
* The tsllmenu plugin
* The tsllsched plugin

But that's not all of it.  The custom teams post type requires some custom fields to operate properly.

_I need to figure out what those things are!_

The real problem here is that I don't remember where to start.  Basically, one of the most important things I need to be able to do is to display the schedule.  The schedule display happens in the template in the tsllsched 

Display Schedule
-----------------
Okay, part one is I need to get the list of games correctly displayed.

The key is the following

1. Create a page inside wordpress and name it Schedule
2. Assign that page the tsllresults page type.
3. Navigate to the page and the schedule should be displayed.

**TODO**: We need to put in the interface to handle displaying the results in a lots of different ways.

Update
--------
the best way to do this is to actually use the plugin to render the content.
We just have access to a lot more code here we can reuse.

	Note: It is helpful to use the onecolumn-page with no side bar template for displaying wide tables.


